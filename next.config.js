const withVideos = require("next-videos");

/** @type {import('next').NextConfig} */

const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: '"i.ibb.co',
        pathname: '**',
      },
    ],
  },
};

module.exports = withVideos(nextConfig);
