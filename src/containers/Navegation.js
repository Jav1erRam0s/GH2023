import React from "react";

import Image from "next/image";
import Favicon from "../sources/images/GH2023.png";
import Informacion from "../sources/images/navbar/informacion.png";
import Jugadores from "../sources/images/navbar/jugadores.png";
import Programas from "../sources/images/navbar/programas.png";

import styles from "../styles/Navegation.module.css";

import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from "reactstrap";

import { Jura } from "next/font/google";

const jura_title = Jura({
  weight: "700",
  subsets: ["latin"],
});

const jura_link = Jura({
  weight: "300",
  subsets: ["latin"],
});

export function Navegation() {
  const [isOpen, setIsOpen] = React.useState(false);

  function toggleCheckbox() {
    if (isOpen == false) {
      setIsOpen(!isOpen);
    } else {
      setIsOpen(!isOpen);
    }
  }

  function closeToggle() {
    setIsOpen(false);
  }

  return (
    <div id={styles.containerNavegation}>
      <Navbar id="navbar" color="dark" expand="lg" fixed="top">
        <NavbarBrand
          href="/#"
          onClick={() => closeToggle()}
          id={styles.navbarbrand}
          className={styles.itemsNavbar}
        >
          <Image
            src={Favicon}
            alt="favicon"
            className={`${"d-none d-sm-none d-md-none d-lg-inline-block"} ${
              styles.imgFavicon
            }`}
          />
          <span className={`${styles.fuenteTitle} ${jura_title.className}`}>
            GH2023
          </span>
        </NavbarBrand>

        <NavbarToggler
          onClick={() => toggleCheckbox()}
          className={styles.navbarTogglerIcon}
        />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ms-auto" navbar>
            <NavItem className="styleItems styleItemEnd">
              <NavLink
                href="#information"
                onClick={() => closeToggle()}
                className={`${styles.marginItemsNavbar} ${styles.itemsNavbar} ${jura_link.className} ${styles.fuenteLink}`}
              >
                <Image
                  src={Informacion}
                  alt="informacion"
                  className={`${styles.imgLink}`}
                />
                Información
              </NavLink>
            </NavItem>
            <NavItem className="styleItems styleItemEnd">
              <NavLink
                href="/#players"
                onClick={() => closeToggle()}
                className={`${styles.marginItemsNavbar} ${styles.itemsNavbar} ${jura_link.className} ${styles.fuenteLink}`}
              >
                <Image
                  src={Jugadores}
                  alt="jugadores"
                  className={`${styles.imgLink}`}
                />
                Jugadores
              </NavLink>
            </NavItem>
            <NavItem className="styleItems styleItemEnd">
              <NavLink
                href="/#programs"
                onClick={() => closeToggle()}
                className={`${styles.lastItemsNavbar} ${styles.itemsNavbar} ${jura_link.className} ${styles.fuenteLink}`}
              >
                <Image
                  src={Programas}
                  alt="programas"
                  className={`${styles.imgLink}`}
                />
                Programas
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}
