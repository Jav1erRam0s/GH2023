import styles from "../styles/Information.module.css";

import Image from "next/image";

import url from "../config/url.js";

import { Jura } from "next/font/google";

const jura_regular = Jura({
  weight: "300",
  subsets: ["latin"],
});

const jura_bold = Jura({
  weight: "700",
  subsets: ["latin"],
});

const fetchInformation = () => {
  return fetch(`${url.path}/information.json`, { cache: "force-cache" }).then(
    (res) => res.json()
  );
};

export async function Information() {
  const information = await fetchInformation();

  return (
    <section
      id="information"
      className={`${styles.ancla} ${jura_regular.className}`}
    >
      <div id={styles.containerInformation}>
        <Image
          src={information.photoHost}
          alt={information.host}
          width={1}
          height={1}
          sizes="100vw"
          className={styles.hostImage}
          style={{
            aspectRatio: "960 / 1440",
          }}
        />

        <div>
          <span className={jura_bold.className}>Conductor: </span>
          {information.host}
        </div>
        <div className={styles.descriptionContainer}>
          <span className={jura_bold.className}>Descripción: </span>
          {information.description}
        </div>
        <div>
          <span className={jura_bold.className}>
            Fecha de Inicio / Finalización:{" "}
          </span>
          {information.startDate.split("-").reverse().join("/")} -{" "}
          {information.endDate.split("-").reverse().join("/")}{" "}
          <span className={`${styles.diasDeJuego} ${jura_bold.className}`}>
            ({information.numberOfDays} dias)
          </span>
        </div>
        <div>
          <span className={jura_bold.className}>Rating (x̄): </span>
          {information.averangeRating}
        </div>
        <div>
          <span className={jura_bold.className}>Jugadores: </span>
          {information.numberOfPlayers}
        </div>
        <div id={styles.tableContainer}>
          <table className="table table-sm rounded-2 overflow-hidden m-0">
            <thead>
              <tr className={`${"table-dark"}`}>
                <th
                  className={`${"text-center"} ${styles.headerNumberTable}`}
                  scope="col"
                >
                  #
                </th>
                <th
                  className={`${"text-center"} ${styles.headerPrizeTable}`}
                  scope="col"
                >
                  Premio
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th className="text-center" scope="row">
                  1°
                </th>
                <td id={styles.textIDTable} className={styles.textClassTable}>
                  {information.firstPrize}{" "}
                  <div className={`${styles.observation}`}>
                    <span className={jura_bold.className}>Observación: </span>
                    {information.observation}
                  </div>
                </td>
              </tr>
              <tr>
                <th className="text-center" scope="row">
                  2°
                </th>
                <td id={styles.textIDTable} className={styles.textClassTable}>
                  {information.secondPrize}
                </td>
              </tr>
              <tr>
                <th className="text-center" scope="row">
                  3°
                </th>
                <td id={styles.textIDTable} className={styles.textClassTable}>
                  {information.thirdPrize}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  );
}
