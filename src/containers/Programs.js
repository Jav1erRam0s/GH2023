import styles from "../styles/Programs.module.css";
import ListPrograms from "../components/ListPrograms.js";

import url from "../config/url.js";

import { Jura } from "next/font/google";

const jura_regular = Jura({
  weight: "300",
  subsets: ["latin"],
});

const jura_bold = Jura({
  weight: "700",
  subsets: ["latin"],
});

const fetchPrograms = () => {
  return fetch(`${url.path}/programs.json`, { cache: "force-cache" }).then(
    (res) => res.json()
  );
};

export async function Programs() {
  var programs = (await fetchPrograms()).reverse();

  return (
    <section id="programs" className={styles.ancla}>
      <div id={styles.containerPrograms}>
        <div className={styles.sectionTitleProgram}>
          <span className={`${styles.titleProgram} ${jura_bold.className}`}>
            Programas
          </span>
          <p className={`${"text-center"} ${jura_regular.className}`}>
            Reviví la programación completa
          </p>
        </div>
        <ListPrograms programs={programs} />
      </div>
    </section>
  );
}
