// "use client";

import styles from "../styles/Presentation.module.css";

import Image from "next/image";

import url from "../config/url.js";
import Link from "next/link";

import BtnAudio from "@/components/BtnAudio";

// import { UncontrolledTooltip } from "reactstrap";

import React from "react";

import { Jura } from "next/font/google";

// const jura_tooltip = Jura({
//   weight: "300",
//   subsets: ["latin"],
// });

const fetchFollowUs = () => {
  return fetch(`${url.path}/follow-us.json`, { cache: "force-cache" }).then(
    (res) => res.json()
  );
};

export async function Presentation() {
  const followUs = await fetchFollowUs();

  return (
    <div id={styles.containerPresentation}>
      <video
        id={styles.presentationVideo}
        autoPlay
        loop
        muted
        src={require("../sources/video/presentacion.mp4")}
      />

      <div className={styles.containerInfo}>
        {followUs.map((element, index) => {
          return (
            <React.Fragment key={index}>
              <Link
                target="_blank"
                rel="noopener noreferrer"
                className={styles.linkFollow}
                href={element.url}
              >
                <Image
                  id={element.name}
                  src={element.icon}
                  alt={element.name}
                  width={1}
                  height={1}
                  sizes="100vw"
                  style={{
                    width: "40px",
                    height: "auto",
                    objectFit: "cover",
                    aspectRatio: "1 / 1",
                  }}
                />
              </Link>

              {/* <UncontrolledTooltip
                delay={0}
                placement="right"
                target={element.name}
                style={{ color: "whitesmoke" }}
              >
                <span className={`${jura_tooltip.className}`}>
                  {element.description}
                </span>
              </UncontrolledTooltip> */}
            </React.Fragment>
          );
        })}
      </div>

      <div id={styles.containerBtnAudio}>
        <BtnAudio />
      </div>
    </div>
  );
}
