import CardPlayer from "@/components/CardPlayer";

import styles from "../styles/Players.module.css";

import url from "../config/url.js";

import { Jura } from "next/font/google";

const jura_regular = Jura({
  weight: "300",
  subsets: ["latin"],
});

const jura_bold = Jura({
  weight: "700",
  subsets: ["latin"],
});

const fetchPlayers = () => {
  return fetch(`${url.path}/players.json`, { cache: "force-cache" }).then(
    (res) => res.json()
  );
};

export async function Players() {
  const players = await fetchPlayers();

  return (
    <section id="players" className={`${styles.ancla} ${jura_regular.className} ${styles.textPlayer}`}>
      <div id={styles.containerPlayers}>
        <span className={`${styles.sectionTitlePlayer} ${jura_bold.className}`}>
          Jugadores
        </span>
        <p>
          Conoce a nuestros participantes, y{" "}
          <a
            href="https://granhermano.mitelefe.com/.El"
            target="_blank"
            rel="noreferrer nofollow noopener"
            className={styles.linkInscription}
          >
            anotate
          </a>{" "}
          a nuestra convocatoria para ser uno de ellos y convertirte en el
          próximo hermanito.
        </p>

        <div className={styles.containerCardsPlayers}>
          <div className={`${"container"} ${"w-85"} ${styles.scrollingWrapper}`}>
            {players.map((element, index) => {
              return <CardPlayer key={index} player={element} />;
            })}
          </div>
        </div>

        <p>
          Recorda que el único requisito para inscribirte en la convocatoria
          para la 10ª edición del reality es ser mayor de 18 años.
        </p>
        <p className="mb-0">
          Dentro de los campos a completar, hay algunos que son obligatorios y
          otros optativos. Entre los primeros, además de la información más
          básica, el aspirante debe contestar cuestiones como si sabe nadar,
          si tiene alguna vinculación con un famoso y qué tipo de música
          escucha. Por otro lado, dentro de la página, hay un espacio para
          dejar el link al video de presentación, que todos los que quieran
          ingresar a Gran Hermano deben grabar. Se trata de una pieza
          audiovisual clave para definir el ingreso de los participantes, ya
          que les permite darse a conocer sin intermediarios. Según las
          directivas del programa, el video puede durar hasta dos minutos y
          ser subido a YouTube.
        </p>
      </div>
    </section>
  );
}
