"use client";

// add bootstrap css
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";

import "./globals.css";
import "normalize.css/normalize.css";

import { Navegation } from "@/containers/Navegation";
import { Presentation } from "@/containers/Presentation";
import { Information } from "@/containers/Information";
import { Players } from "@/containers/Players";
import { Programs } from "@/containers/Programs";

import { useEffect } from "react";

export default function RootLayout({ children }) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);

  return (
    <html lang="en">
      <head>
        <title>GH2023</title>
      </head>
      <body>
        {/* {children} */}
        <Navegation />
        <Presentation />
        <Information />
        <Players />
        <Programs />
      </body>
    </html>
  );
}
