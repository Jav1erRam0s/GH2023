import styles from "../styles/Pagination.module.css";

import Image from "next/image";
import Previou from "../sources/images/body/previou.png";
import Next from "../sources/images/body/next.png";

import { Jura } from "next/font/google";

const jura_bold = Jura({
  weight: "700",
  subsets: ["latin"],
});

export const Pagination = ({
  paginaActual,
  cantidadDePaginas,
  updatePage,
}) => {
  /* Metodo de paginacion */
  function previousPage() {
    const nuevaPage = paginaActual - 1;
    if (nuevaPage >= 1) {
      updatePage(nuevaPage);
    }
  }
  function nextPage() {
    const nuevaPage = paginaActual + 1;
    if (nuevaPage <= cantidadDePaginas) {
      updatePage(nuevaPage);
    }
  }

  return (
    <footer id={styles.footer}>
      <div
        id={styles.footerContainer}
        className="d-flex flex-row justify-content-center align-items-center"
      >
        {/* Button Previou */}
        <a href="#programs" className={styles.anclaje}>
          <div
            className={`${styles.containerMain}`}
            onClick={() => previousPage()}
          >
            <div className={`${styles.containerControl}`}>
              <Image
                src={Previou}
                alt="previou"
                className={styles.imgControl}
              />
            </div>
          </div>
        </a>
        {/* Info */}
        <div className={`${styles.infoPages} ${jura_bold.className}`}>
          {paginaActual}/{cantidadDePaginas}
        </div>
        {/* Button Next */}
        <a href="#programs" className={styles.anclaje}>
          <div
            className={`${styles.containerMain}`}
            onClick={() => nextPage()}
          >
            <div className={`${styles.containerControl}`}>
              <Image
                src={Next}
                alt="next"
                className={styles.imgControl}
              />
            </div>
          </div>
        </a>
      </div>
    </footer>
  );
};
