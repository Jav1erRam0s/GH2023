import React from "react";

import Image from "next/image";
import styles from "../styles/CardProgram.module.css";

import { ShowProgram } from "./ShowProgram";

import { Jura } from "next/font/google";

const jura_regular = Jura({
  weight: "300",
  subsets: ["latin"],
});

function showType(type) {
  if (type == "La Gran Final") {
    return (
      <span className={`${"badge rounded-pill text-bg-primary"} ${styles.footerType}`}>
        {type}
      </span>
    );
  } else if (type == "Gala de Nominación") {
    return (
      <span className={`${"badge rounded-pill text-bg-warning"} ${styles.footerType}`}>
        {type}
      </span>
    );
  } else if (type == "La Noche de los Ex") {
    return (
      <span className={`${"badge rounded-pill text-bg-success"} ${styles.footerType}`}>
        {type}
      </span>
    );
  } else if (type == "Gala de Eliminación") {
    return (
      <span className={`${"badge rounded-pill text-bg-danger"} ${styles.footerType}`}>
        {type}
      </span>
    );
  } else if (type == "Repechaje") {
    return (
      <span className={`${"badge rounded-pill text-bg-light"} ${styles.footerType}`}>
        {type}
      </span>
    );
  } else if (type == "Expulsión") {
    return (
      <span className={`${"badge rounded-pill text-bg-dark"} ${styles.footerType}`}>
        {type}
      </span>
    );
  } else if (type == "") {
    return <span></span>;
  }
}

export default function CardProgram({ program }) {
  const [stateModal, setStateModal] = React.useState(false);

  function openModal() {
    var audio = document.getElementById("clickCardProgram");
    audio.play();
    setStateModal(true);
  }

  function closeModal() {
    setStateModal(false);
  }
  
  return (
    <React.Fragment>
    <audio id="clickCardProgram" src="/audio/click-card-program.mp3" />

      <div className={`${styles.containerCardProgram} ${jura_regular.className}`}>
        <div className={styles.containerImages}>
          <Image
            src={program.photo}
            alt={program.number}
            width={1}
            height={1}
            sizes="100vw"
            className={styles.programImage}
          />
          <svg className={styles.playImage} onClick={openModal} height="100%" width="100%" viewBox="0 0 68 48" ><path d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#f00"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg>
        </div>

        <div className={`${styles.recomendacionFooter} ${jura_regular.className}`}>
          {showType(program.type)}
          <span className={styles.footerDate}> {program.date.split("-").reverse().join("/")} </span>
        </div>
      </div>
      
      <ShowProgram
        program={program}
        stateModal={stateModal}
        closeModal={closeModal}
      />
    </React.Fragment>
  );
}
