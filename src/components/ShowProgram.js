import React from "react";
import { Modal, ModalHeader } from "reactstrap";

import styles from "../styles/ShowProgram.module.css";

import { Jura } from "next/font/google";

const jura_regular = Jura({
  weight: "300",
  subsets: ["latin"],
});

export const ShowProgram = ({ program, stateModal, closeModal }) => {
  return (
    <React.Fragment>
      <Modal contentClassName={styles.classModalContent} size="lg" isOpen={stateModal} centered>
        <ModalHeader
          toggle={closeModal}
          cssModule={{ "modal-title": "w-100 text-center m-0" }}
          className={styles.modalHeader}
        >
          <div className={`${styles.modalHeaderContent} ${jura_regular.className}`}>
            <span>
              🧿 {'Programa N° '+program.number}
            </span>
            <span>
              📅 {program.date.split("-").reverse().join("/")}
            </span>
          </div>
        </ModalHeader>

        <iframe id={styles.iframeProgram} src={program.url+"?autoplay=1&mute=0&loop=0"} title={program.type}></iframe>
      </Modal>
    </React.Fragment>
  );
};
