"use client";

import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Jura } from "next/font/google";

import styles from "../styles/DetailsPlayer.module.css";

import Image from "next/image";

import { Tooltip } from 'reactstrap';

const jura_regular = Jura({
  weight: "300",
  subsets: ["latin"],
});

const jura_bold = Jura({
  weight: "700",
  subsets: ["latin"],
});

export const DetailsPlayer = ({ player, stateModal, closeModal }) => {

  const [tooltipEstado, setTooltipEstado] = React.useState(false);
  const toggleEstado = () => setTooltipEstado(!tooltipEstado);
  
  const [tooltipEstadia, setTooltipEstadia] = React.useState(false);
  const toggleEstadia = () => setTooltipEstadia(!tooltipEstadia);

  function calculateAge(date) {
    var currentDate = new Date();
    var birthdate = new Date(date);
    var year = currentDate.getFullYear() - birthdate.getFullYear();
    var m = currentDate.getMonth() - birthdate.getMonth();
    if (m < 0 || (m === 0 && currentDate.getDate() < birthdate.getDate())) {
        year--;
    }
    return year;
  }

  function showState(state){
    if(state == "Ganador") {
      return <span className={`${"badge rounded-pill text-bg-primary"} ${styles.modalEstadoContainer}`}>{state}</span>;
    }
    else if(state == "2° Puesto") {
      return <span className={`${"badge rounded-pill text-bg-warning"} ${styles.modalEstadoContainer}`}>{state}</span>
    }
    else if(state == "3° Puesto") {
      return <span className={`${"badge rounded-pill text-bg-warning"} ${styles.modalEstadoContainer}`}>{state}</span>
    }
    else if(state.includes("Eliminad")) {
      return <span className={`${"badge rounded-pill text-bg-danger"} ${styles.modalEstadoContainer}`}>{state}</span>
    }
    else if(state.includes("Expulsad")) {
      return <span className={`${"badge rounded-pill text-bg-dark"} ${styles.modalEstadoContainer}`}>{state}</span>
    }
  }

  return (
    <React.Fragment>
      <Modal contentClassName={styles.classModalContent} size="lg" scrollable={true} isOpen={stateModal} centered>

        <ModalHeader
          toggle={closeModal}
          cssModule={{ "modal-title": "w-100 text-center m-0" }}
          className={styles.modalHeader}
        >
          <div className={`${styles.modalHeaderText} ${jura_bold.className}`}>
            {player.nick[0]}
          </div>
        </ModalHeader>

        <ModalBody className={`${styles.modalBody} ${jura_regular.className}`}>
          <div className={styles.modalInfoContainer}>
            <Image
              src={player.photo}
              alt={player.name}
              width={1}
              height={1}
              sizes="100vw"
              className={styles.modalImage}
            />
            <div><span className={jura_bold.className}>Nombre: </span>{player.name}</div>
            <div><span className={jura_bold.className}>Apodo: </span>{player.nick.map( (nick, index, arr) => { if(index === arr.length - 1) { return nick } else { return nick + " • " }} )}</div>
            <div><span className={jura_bold.className}>Fecha de Nacimiento: </span>{player.birthdate.split('-').reverse().join('/')} <span className={`${styles.anioCumplidos} ${jura_bold.className}`}>({calculateAge(player.birthdate)} años)</span></div>
            <div className={styles.modalDescriptionContainer}><span className={jura_bold.className}>Descripción: </span>{player.description}</div>
          </div>

          <div className="accordion" id={styles.accordionRedesSociales}>
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button className={`${styles.headerAccordion} ${"accordion-button collapsed"} ${jura_bold.className}`} type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                  Redes Sociales
                </button>
              </h2>
              <div id="collapseOne" className="accordion-collapse collapse" data-bs-parent="#accordionRedesSociales">
                <div className="accordion-body">
                  <ul className={`${styles.modalRedesContainer}`}>
                    {player.networks.map((element, index) => {
                      return <li key={index}><a href={element} className={styles.modalRedesLink} rel="noreferrer nofollow noopener" target="_blank">{element}</a></li>
                      })}
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.modalPresentacionContainer}>
            <div className={`${styles.modalPresentacionTitle} ${jura_bold.className}`}>Presentación</div>
            <iframe className={styles.modalPresentacionIframe} src={player.presentation+'?autoplay=0&mute=0&loop=0'} title={player.name}/>
          </div>

        </ModalBody>

        <ModalFooter className={`${styles.modalFooter} ${jura_regular.className}`}>
          <div className={`${"row"}`} style={{ width: "100%", margin: "0" }}>
            <div id="tooltipEstado" className={`${"col-6"} ${styles.alignContentInfoPlayer}`}>
              {showState(player.state)}
            </div>
            <div id="tooltipEstadia" className={`${"col-6"} ${styles.alignContentInfoPlayer}`}>
              <span>🏠 {player.days} dias</span>
            </div>
          </div>
        </ModalFooter>

        <Tooltip className={jura_regular.className} placement="top" isOpen={tooltipEstado} target="tooltipEstado" toggle={toggleEstado}>
          <span className={jura_regular.className}>Estado en el juego</span>
        </Tooltip>

        <Tooltip placement="top" isOpen={tooltipEstadia} target="tooltipEstadia" toggle={toggleEstadia}>
          <span className={jura_regular.className}>Estadía en GH</span>
        </Tooltip>

      </Modal>
    </React.Fragment>
  );
};
