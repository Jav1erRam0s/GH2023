"use client";

import React from "react";
import CardProgram from "./CardProgram";
import { Pagination } from "./Pagination";

function ListPrograms({ programs }) {
  const [programas] = React.useState(programs);
  const [cantidadProgramasPage] = React.useState(12);
  const [actualPage, setActualPage] = React.useState(1);
  const [programasPage, setProgramasPage] = React.useState(
    programas.slice(
      (actualPage - 1) * cantidadProgramasPage,
      actualPage * cantidadProgramasPage
    )
  );
  const cantidadDePaginas = Math.ceil(programas.length / cantidadProgramasPage);
  var list = [];
  for (var i = 1; i <= cantidadDePaginas; i++) {
    list.push(i);
  }
  const [paginacion] = React.useState(list);

  /* Metodo de paginacion */
  function updatePage(page) {
    setActualPage(page);
    setProgramasPage(
      programas.slice(
        (page - 1) * cantidadProgramasPage,
        page * cantidadProgramasPage
      )
    );
  }

  return (
    <React.Fragment>
      {/* LISTA DE PROGRAMAS */}
      <section className="row">
        {programasPage.map((element, index) => {
          return (
            <span className="col-6 col-md-4 col-lg-3 mb-4" key={index}>
              <CardProgram program={element} />
            </span>
          );
        })}
      </section>
      {/* PAGINACION */}
      <Pagination paginaActual={actualPage} cantidadDePaginas={paginacion.length} updatePage={updatePage} />
    </React.Fragment>
  );
}

export default ListPrograms;
