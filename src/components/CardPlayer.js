import React from "react";

import { DetailsPlayer } from "./DetailsPlayer";

import styles from "../styles/CardPlayer.module.css";

import Image from "next/image";

import { Jura } from "next/font/google";

const jura = Jura({
  weight: "300",
  subsets: ["latin"],
});

export default function CardPlayer({ player }) {
  const [stateModal, setStateModal] = React.useState(false);

  function playHoverPlayer() {
    var audio = document.getElementById("hoverCardPlayer");
    audio.play();
  }

  function openModal() {
    setStateModal(true);
  }

  function closeModal() {
    setStateModal(false);
  }

  return (
    <React.Fragment>
      <audio id="hoverCardPlayer" src="/audio/hover-card-player.mp3" />

      <div className={styles.borderCard}>
        <div className={styles.imageCard} onMouseEnter={playHoverPlayer} onClick={openModal}>
          <Image
            src={player.photo}
            alt={player.name}
            width={1}
            height={1}
            sizes="100vw"
            className={styles.playerImage}
          />
          <div className={`${styles.namePlayer} ${jura.className}`}>
            {player.nick[0]}
          </div>
        </div>

        <DetailsPlayer
          player={player}
          stateModal={stateModal}
          closeModal={closeModal}
        />
      </div>
    </React.Fragment>
  );
}
