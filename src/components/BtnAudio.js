import React from "react";

import styles from "../styles/BtnAudio.module.css";

import Image from "next/image";
import VolumeUp from "../sources/images/presentacion/volume-up.png";
import VolumeDown from "../sources/images/presentacion/volume-down.png";

function BtnAudio() {
  const [stateImgAudio, setStateImgAudio] = React.useState(VolumeDown);
  const [stateAudio, setStateAudio] = React.useState(true);

  function onOffAudio() {
    var audio = document.getElementById("audioCortinaMusical");

    if (stateAudio) {
      setStateAudio(false);
      setStateImgAudio(VolumeUp);
      audio.pause();
    } else {
      setStateAudio(true);
      setStateImgAudio(VolumeDown);
      audio.play();
    }
  }

  return (
    <React.Fragment>
      <audio
        id="audioCortinaMusical"
        autoPlay
        loop
        src="/audio/cortina-musical-gh.mp3"
      />

      <div id={styles.btnAudioGH} onClick={onOffAudio}>
        <Image
          id="imgAudioPresentation"
          src={stateImgAudio}
          alt="imgAudio"
          width={20}
          height={20}
          sizes="100vw"
        />
      </div>
    </React.Fragment>
  );
}

export default BtnAudio;
